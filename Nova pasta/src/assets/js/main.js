;(function(window, document, $) {
	var $win = $(window);
	var $doc = $(document);

	// Initialize slider intro
	$('.slider--intro .slider__slides').slick({
		infinite: true,
		slidesToShow: 1,
		fade: true,
		slidesToScroll: 1,
		dots: true
	});

	// Initialize slider products
	$('.slider--products .slider__slides').slick({
		infinite: true,
		slidesToScroll: 1,
		dots: true,
		slidesToShow: 2,
		initialSlide: 1,
		responsive: [{
            breakpoint: 768,
            settings: {
               slidesToShow: 1,
            }
        }]
	});
	var  $sliderNav = $('.slider__nav');
	var  $sliderImages = $('.slider--images .slider__slides');

	// Initialize slider images
	 $sliderImages.slick({
		slidesToShow: 1,
	    slidesToScroll: 1,
	    fade: true,
	    responsive: [{
            breakpoint: 99999,
            dots: false
        },{
            breakpoint: 768,
            settings: {
               dots: true
            }
        }]
	});

	$sliderNav.on('click', 'a', function(event) {
	    var $li = $(this).parent();
	    var idx = $li.index();

    	$sliderImages.slick('slickGoTo', idx);

	    event.preventDefault();
	});

	$sliderImages.on('beforeChange', function(event, slick, currentSlide, nextSlide) {

	    $sliderNav.find('li').eq(nextSlide).addClass('current').siblings().removeClass('current');
	});

	// Initialize slider images tertiary
	$('.slider--products-tertiary .slider__slides').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		dots: true
	});

	// Initialize slider images secondary
	$('.slider--images-secondary .slider__slide-image').on('click', function(event) {
		$(this).closest('.slick-slide').addClass('slick-current').siblings().removeClass('slick-current');

		event.preventDefault();
	});

	$('.slider--images-secondary .slider__slides').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		responsive: [{
            breakpoint: 768,
            settings: {
               slidesToShow: 4,
            }
        }]
	});

	$('.btn-link').on('click', function(event) {
	    event.preventDefault();
	});

	//Open select dropdown
	$('.select--primary > a').on('click touchstart', function(event) {
	    $(this).closest('.select--primary').toggleClass('visible')
	    	.siblings().removeClass('visible');

	    event.preventDefault();
	});

	//Close select dropdown
	$doc.on('click touchstart', function(event) {
		var $target = $(event.target);

		if (!$target.closest('.select--primary').length) {
	   		$('.select--primary').removeClass('visible');
		}
	});

	//Open filters
	$('.btn--filters').on('click', function(event) {
    	$('.filters--wrapper').addClass('visible');

	    event.preventDefault();
	});

	//Close filters
	$('.btn--close-filters').on('click', function(event) {
    	$('.filters--wrapper').removeClass('visible');

	    event.preventDefault();
	});

	$('.link-order').on('click', function(event) {
		$('.order').addClass('visible');

		event.preventDefault();
	});

	//Open filters
	$('.btn--close-order').on('click', function(event) {
    	$('.order').removeClass('visible');

	    event.preventDefault();
	});


	$('.select--brand').find('.dropdown__inner-content').clone().appendTo('#accordionFilters .card--brand .card-body');
	$('.select--category').find('.dropdown__inner-content').clone().appendTo('#accordionFilters .card--category .card-body');
	$('.select--colors').find('.dropdown__inner-content').clone().appendTo('#accordionFilters .card--colors .card-body');
	$('.select--size').find('.dropdown__inner-content').clone().appendTo('#accordionFilters .card--size .card-body');
	$('.select--price').find('.dropdown__inner-content').clone().appendTo('#accordionFilters .card--price .card-body');

	// Selectric
	$('.select--main select').selectric({
		disableOnMobile: false,
  		nativeOnMobile: false
	});

	$win.on('load', function() {

		$('.details__title').clone().insertBefore('.slider--images');

		$('.footer__inner-content').clone().insertAfter('.list--cards').removeClass('hidden-xs');

	}).on('load resize', function() {
		if ($win.width() <= 767) {
			//Close filters
			$('.btn--close-order').on('click', function(event) {
		    	$('.order').removeClass('visible');

			    event.preventDefault();
			});

			// Open cart
			$('.cart__btn').on('click', function(event) {
				$('.cart__content').addClass('visible');

				$('.nav-trigger').removeClass('nav-trigger--active');
				$('.nav-wrapper').removeClass('visible');

				event.preventDefault();
			});

			$('.cart__btn-close').on('click', function(event) {
				$('.cart__content').removeClass('visible');

				event.preventDefault();
			});


			// Open mobile navigation
			$('.nav-trigger').on('click', function(event) {
				$(this).toggleClass('nav-trigger--active');
				$('.nav-wrapper').toggleClass('visible');

				$('.search').removeClass('visible');
				$('.cart__content').removeClass('visible');
				$('.profile__inner').removeClass('visible');

				event.preventDefault();
			});

			// Check for dropdown
			$('.nav li').each(function() {
				 var $this = $(this);
				 var hasDropdown = $this.find('> .dropdown').length;

				 if (hasDropdown) {
				 	$this.addClass('has-dropdown');
				 }
			});

			// Rsponsive navigation
			$('.nav-wrapper').on('click', '.has-dropdown a', function() {
				if ($win.width() < 768) {
					$(this).siblings('.dropdown').stop().slideToggle()
						.parent().toggleClass('is-expanded');
					$(this).parent().siblings().removeClass('is-expanded');
					$(this).parent().siblings().find('.dropdown').slideUp();

					event.preventDefault();
				}
			});

			// Slider socials

			$('.slider--socials .slider__slides:not(.slick-initialized)').slick({
			    slidesToScroll: 1,
				slidesToShow: 1,
			    infinite: true,
			    initialSlide: 1,
		      	responsive: [{
		            breakpoint: 99999,
		            settings: "unslick"
		        },{
		            breakpoint: 768,
		            settings: {
		                dots: true,
		            }
		        }]
			});

			$('.slider--products-secondary .slider__slides:not(.slick-initialized)').slick({
			    slidesToScroll: 1,
				slidesToShow: 1,
			    infinite: true,
		      	responsive: [{
		            breakpoint: 99999,
		            settings: "unslick"
		        },{
		            breakpoint: 768,
		            settings: {
		                dots: true,
		            }
		        }]
			});

			$('.slider--colors .slider__slides:not(.slick-initialized)').slick({
			    slidesToScroll: 6,
				slidesToShow: 6,
			    infinite: true,
		      	responsive: [{
		            breakpoint: 99999,
		            settings: "unslick"
		        },{
		            breakpoint: 768,
		            settings: {
		                dots: false,
		            }
		        }]
			});
		}
	});
})(window, document, window.jQuery);

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "../src/assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../src/assets/js/main.js":
/*!********************************!*\
  !*** ../src/assets/js/main.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

;

(function (window, document, $) {
  var $win = $(window);
  var $doc = $(document);
  $('.slider--intro .slider__slides').slick({
    infinite: true,
    slidesToShow: 1,
    fade: true,
    slidesToScroll: 1,
    dots: true
  });
  $('.slider--products .slider__slides').slick({
    infinite: true,
    slidesToScroll: 1,
    dots: true,
    slidesToShow: 2,
    initialSlide: 1,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 1
      }
    }]
  });
  $('.slider--images .slider__slides').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    infinite: true,
    initialSlide: 1,
    responsive: [{
      breakpoint: 99999,
      dots: false
    }, {
      breakpoint: 768,
      settings: {
        dots: true
      }
    }]
  });
  $('.slider--products-tertiary .slider__slides').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: true
  });
  $('.slider--images-secondary .slider__slides').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 4
      }
    }]
  });
  $('.slider__nav').on('click', 'a', function (event) {
    var $li = $(this).parent();
    var idx = $li.index();
    $('.slider--images .slider__slides').slick('slickGoTo', idx);
    event.preventDefault();
  });
  $('.slider--images .slider__slides').on('afterChange', function (slick, currentSlide) {
    var activeIdx = $(this).find('.slick-active').index();
    $('.slider__nav').find('li').eq(activeIdx).addClass('current').siblings().removeClass('current');
  });
  $('.select--main select').selectric({
    disableOnMobile: false,
    nativeOnMobile: false
  });
  $win.on('load', function () {
    $('.details__title').clone().insertBefore('.slider--images');
    $('.footer__inner-content').clone().insertAfter('.list--cards').removeClass('hidden-xs');
  }).on('load resize', function () {
    if ($win.width() <= 767) {
      $('.cart__btn').on('click', function (event) {
        $('.cart__content').addClass('visible');
        $('.nav-trigger').removeClass('nav-trigger--active');
        $('.nav-wrapper').removeClass('visible');
        event.preventDefault();
      });
      $('.cart__btn-close').on('click', function (event) {
        $('.cart__content').removeClass('visible');
        event.preventDefault();
      });
      $('.nav-trigger').on('click', function (event) {
        $(this).toggleClass('nav-trigger--active');
        $('.nav-wrapper').toggleClass('visible');
        $('.search').removeClass('visible');
        $('.cart__content').removeClass('visible');
        $('.profile__inner').removeClass('visible');
        event.preventDefault();
      });
      $('.nav li').each(function () {
        var $this = $(this);
        var hasDropdown = $this.find('> .dropdown').length;

        if (hasDropdown) {
          $this.addClass('has-dropdown');
        }
      });
      $('.nav-wrapper').on('click', '.has-dropdown a', function () {
        if ($win.width() < 768) {
          $(this).siblings('.dropdown').stop().slideToggle().parent().toggleClass('is-expanded');
          $(this).parent().siblings().find('.dropdown').slideUp();
          event.preventDefault();
        }
      });
      $('.slider--socials .slider__slides:not(.slick-initialized)').slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        infinite: true,
        initialSlide: 1,
        responsive: [{
          breakpoint: 99999,
          settings: "unslick"
        }, {
          breakpoint: 768,
          settings: {
            dots: true
          }
        }]
      });
      $('.slider--products-secondary .slider__slides:not(.slick-initialized)').slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        infinite: true,
        responsive: [{
          breakpoint: 99999,
          settings: "unslick"
        }, {
          breakpoint: 768,
          settings: {
            dots: true
          }
        }]
      });
      $('.slider--colors .slider__slides:not(.slick-initialized)').slick({
        slidesToScroll: 1,
        slidesToShow: 6,
        infinite: true,
        responsive: [{
          breakpoint: 99999,
          settings: "unslick"
        }, {
          breakpoint: 768,
          settings: {
            dots: false
          }
        }]
      });
    }
  });
})(window, document, window.jQuery);

/***/ })

/******/ });